class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :destroy]

  def index
    ip_address = request.remote_ip
    @image = Image.new
    @images = Image.where(ip_address: ip_address)
  end

  def show
    incremented_views_count = @image.views + 1
    @image.update!(views: incremented_views_count)
    data = open(@image.attachement_url)
    send_data data.read, filename: "#{@image.id}.#{@image.attachement.file.extension}", type: "image/#{@image.attachement.file.extension}", disposition: 'inline'
  end

  def create
    @image = Image.new(attachement: params[:attachement])
    @image.ip_address = request.remote_ip
    respond_to do |format|
      if @image.save
        format.json { render json: { html_content: render_to_string(partial: 'images/image.html', locals: {image: @image}) }, status: :created }
      else
        format.json { render json: { errors: @image.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @image.destroy!
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def set_image
    @image = Image.find(params[:id])
  end

end
