class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.string :attachement
      t.string :ip_address
      t.integer :views, default: 0

      t.timestamps
    end
  end
end
